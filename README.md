# Auth Json Web Token Mern

## Hey dude, I added this GitLab project to an auto-build on "push" system! 😎

*Live Build URL*: [auth-token.monkeyisland.com.au](https://auth-token.monkeyisland.com.au/)

Any time you commit your changes and do a "Push" a server somewhere will build all your node shit and within a few seconds it will be at that URL above.

...auto-magically 🐳🌅🏄🌊 ✌(◕‿-)✌ 🏝️🌄🏝️

## This is a create-react-app and nodejs app

.env in store root,

```env
PORT=3000
HTTPS=true
REACT_APP_JWT_SECRET="key"
```

key.json in actual node root,

```json
{
    "key":"key"
}
```

cd ???/certsFiles

sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout ./selfsigned.key -out selfsigned.crt

https://github.com/facebook/create-react-app/issues/752