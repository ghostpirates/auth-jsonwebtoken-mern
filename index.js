let bcrypt = require('bcrypt')
let jwt = require('jsonwebtoken')
let jsonn = require('./key.json')

let cookieParser = require('cookie-parser')

let fs = require('fs')
let key = fs.readFileSync(__dirname + '/certsFiles/selfsigned.key')
let cert = fs.readFileSync(__dirname + '/certsFiles/selfsigned.crt')
let credentials = {
    key: key,
    cert: cert
}

const express = require('express')
const app = express()
let https = require('https').createServer(credentials, app)
let httpsPort = 3002
https.listen(httpsPort, () => {
    console.log("Https server listing on port : " + httpsPort)
})


let bodyParser = require('body-parser')
app.use(cookieParser())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "https://auth-token.monkeyisland.com.au/") // update to match the domain you will make the request from
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
    next()
})

let mongoose = require('mongoose')
let sch = mongoose.Schema({
    username: String,
    password: String
})
let mod = mongoose.model('users', sch)
mongoose.connect('mongodb://localhost/use', {
    useNewUrlParser: true,
    useUnifiedTopology: true
})

app.post('/logUp', function (req, res) {
    if (!req.body.token) {
        return res.status(404).json({ message: 'No pass token found.' })
    } else {
        let use = jwt.verify(req.body.token, jsonn.key)
        let hash = bcrypt.hashSync(use.password.trim(), 10)
        mod.find({ username: use.username.trim() }).exec(function (err, user) {
            if (!user.length) {
                let b = new mod({ username: use.username.trim(), password: hash })
                b.save(function (err, users) {
                    let token = jwt.sign({ user: users.username, pass: hash }, jsonn.key, { expiresIn: 60 * 2 })
                    return res.cookie('token', { tokenres: token }, { expires: new Date(Date.now() + 50000), secure: true, httpOnly: true }).json({ tokenres: token })
                })
            }
            else {
                return res.status(404).json({ message: 'Username is taken.' })
            }
        })
    }
})

app.post('/logIn', function (req, res) {
    if (!req.body.token) {
        return res.status(404).json({ message: 'No pass token found.' })
    } else {
        let users = jwt.verify(req.body.token, jsonn.key)
        mod.findOne({ username: users.username.trim() })
            .exec(function (err, user) {
                if (!user) {
                    return res.status(404).json({ message: 'Username or password is wrong.' })
                }
                bcrypt.compare(users.password, user.password,
                    function (err, valid) {
                        if (!valid) {
                            return res.status(404).json({ message: 'Username or password is wrong.' });
                        } else if (valid) {
                            let token = jwt.sign({ user: user.username, pass: user.password }, jsonn.key, { expiresIn: 60 * 2 })
                            return res.cookie('token', { tokenres: token }, { expires: new Date(Date.now() + 50000), secure: true, httpOnly: true }).json({ tokenres: token })
                        }
                    })
            })
    }
})

app.post('/tok', function (req, res) {
    if (req.cookies.token) {
        jwt.verify(req.cookies.token.tokenres, jsonn.key, function (err, user) {
            mod.findOne({ username: user.user.trim() }).exec(function (err, users) {
                if (!user) {
                    return res.status(404).json({ message: 'Please login or sign up.' })
                } else {
                    if (users.password === user.pass) {
                        let token = jwt.sign({ user: users.username }, jsonn.key, { expiresIn: 60 * 2 })
                        return res.json({ tokenres: token })
                    } else {
                        return res.status(404).json({ message: 'Username or password is wrong.' })                             
                    }
                }
            })
        })
    } else {
        return res.status(404).json({ message: 'Please login or sign up.' })
    }
})

app.post('/do', function (req, res) {
    if (!req.body.token) {
        return res.status(404).json({ message: 'No pass token found.' })
    } else {
        jwt.verify(req.body.token, jsonn.key, function (err, user) {
            if (!user) {
                return res.status(404).json({ message: 'Username or password is wrong.' })
            } else {
                let token = jwt.sign({ db: Math.random() }, jsonn.key, { expiresIn: 60 * 2 })
                return res.json({ tokenres: token })
            }
        })
    }
})

app.post('/clear', function (req, res) {  
    res.clearCookie('token')
    res.end()
})
