import React from 'react';
import ReactDOM from 'react-dom'
import axios from 'axios'

import jwt from 'jsonwebtoken'



class Index extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            password: '',
            username: ''           
        }
    }
    componentDidMount() {        
        axios.post(`https://auth-token.monkeyisland.com.au/tok`)
        .then(res => {
           let use = jwt.verify(res.data.tokenres, process.env.REACT_APP_JWT_SECRET)
            alert('User: ' + use.user)
        }).catch(error => {
            alert(error.response.data.message)
        })            
    }
    change = (event) => {
        let nam = event.target.name
        let val = event.target.value
        this.setState({ [nam]: val })
    }
    sub = (event) => {
        event.preventDefault()
        let token = jwt.sign({ username: this.state.username, password: this.state.password }, process.env.REACT_APP_JWT_SECRET, { expiresIn: 60 * 2 })
        axios.post(`https://auth-token.monkeyisland.com.au/logUp`, { token: token })
            .then(res => {
                let use = jwt.verify(res.data.tokenres, process.env.REACT_APP_JWT_SECRET)
                alert('Sign up: ' + use.user)
            })
            .catch(error => {
                alert(error.response.data.message)
            })
        document.getElementById("a1").reset()
    }
    sub0 = (event) => {
        event.preventDefault()
        let token = jwt.sign({ username: this.state.username, password: this.state.password }, process.env.REACT_APP_JWT_SECRET, { expiresIn: 60 * 2 })
        axios.post(`https://auth-token.monkeyisland.com.au/logIn`, { token: token })
            .then(res => {
                let use = jwt.verify(res.data.tokenres, process.env.REACT_APP_JWT_SECRET)
                alert('Login: ' + use.user)
                return use.user
            })
            .then(res => {
                let token = jwt.sign({ username: res }, process.env.REACT_APP_JWT_SECRET, { expiresIn: 60 * 2 })
                axios.post(`https://auth-token.monkeyisland.com.au/do`, { token: token })
                    .then(res => {
                        let use = jwt.verify(res.data.tokenres, process.env.REACT_APP_JWT_SECRET)
                        alert(use.db)
                    }).catch(error => {
                        alert(error.response.data.message)
                    })

            }).catch(error => {
                alert(error.response.data.message)
            })
        document.getElementById("a2").reset()
    }
    out = () => {
        axios.post(`https://auth-token.monkeyisland.com.au/clear`)        
    }   
    render() {
        return (<div>
            <form id='a1' onSubmit={this.sub}>
                <input type="text" name="username" placeholder='UserName:' onChange={this.change} />
                <input type="text" name="password" placeholder='Password:' onChange={this.change} />
                <button type="submit" > Sign Up</button>
            </form>
            <form id='a2' onSubmit={this.sub0}>
                <input type="text" name="username" placeholder='UserName:' onChange={this.change} />
                <input type="text" name="password" placeholder='Password:' onChange={this.change} />
                <button type="submit" > Sign In</button>
            </form>
            <button onClick={this.out}> Log out</button>            
        </div>
        )
    }
}
ReactDOM.render(<Index />, document.getElementById('root'));

